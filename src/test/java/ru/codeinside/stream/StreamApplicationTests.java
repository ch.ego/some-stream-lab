package ru.codeinside.stream;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.codeinside.stream.entity.Customer;
import ru.codeinside.stream.entity.Order;
import ru.codeinside.stream.entity.Product;
import ru.codeinside.stream.repository.CustomerRepository;
import ru.codeinside.stream.repository.OrderRepository;
import ru.codeinside.stream.repository.ProductRepository;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@DataJpaTest
class StreamApplicationTests {
    private final CustomerRepository customerRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public StreamApplicationTests(CustomerRepository customerRepository, ProductRepository productRepository, OrderRepository orderRepository) {
        this.customerRepository = customerRepository;
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
    }

    @Test
    @DisplayName("tack1: Получить список товаров в категории = \"Книги\" у которых цена > 1000")
    void tack1() {
        List<Product> result = productRepository.findAll()
                .stream()
                .filter(product -> Objects.equals(product.getCategory(), "Книги") && product.getPrice() > 1000)
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }


    @Test
    @DisplayName("tack2: Получить список товаров в категории = \"Книги\" у которых цена > 1000, используйте Predicate для фильтра")
    void tack2() {
        Predicate<Product> categoryFilter = (product -> Objects.equals(product.getCategory(), "Книги"));
        Predicate<Product> priceFilter = (product -> product.getPrice() > 1000);
        List<Product> result = productRepository.findAll()
                .stream()
                .filter(categoryFilter)
                .filter(priceFilter)
                .toList();
        result.forEach(p -> log.info(p.toString()));
    }


    @Test
    @DisplayName("tack3: Получить список заказов товара с категорией = \"Товары для дома\"")
    void tack3() {
        List<Order> result = orderRepository.findAll()
                .stream()
                .filter(order ->
                        order.getProducts()
                                .stream()
                                .anyMatch(product -> Objects.equals(product.getCategory(), "Товары для дома")))
                .toList();
        result.forEach(o -> log.info(o.toString()));
    }

    @Test
    @DisplayName("tack4: Получить список товаров с категорией = “Игры”, а затем применить скидку 5% к каждой позиции в списке\"")
    void tack4() {
        List<Product> result = productRepository.findAllByCategory("Игры").stream()
                .peek(product -> product.setPrice(product.getPrice() * 0.95))
                .toList();
        result.forEach(o -> log.info(o.toString()));

    }

    @Test
    @DisplayName("tack5: Получить список продуктов, заказанных клиентом c уровнем 2 в период с 01 марта 2022 года по 01 апреля 2022 года")
    void tack5() {
        List<Product> result = new ArrayList<>();
        orderRepository.findAll()
                .stream()
                .filter(order -> order.getCustomer().getLevel() == 2
                        && order.getOrderDate().isAfter(LocalDate.of(2022, 3, 1))
                        && order.getOrderDate().isBefore(LocalDate.of(2022, 4, 2)))
                .map(Order::getProducts)
                .forEach(result::addAll);
        result.forEach(o -> log.info(o.toString()));
    }

    @Test
    @DisplayName("tack6: Получите 3 самых дешевых товара из категории \"Товары для дома\"")
    void tack6() {
        List<Product> result = productRepository.findAll()
                .stream()
                .filter(product -> Objects.equals(product.getCategory(), "Товары для дома"))
                .sorted(Comparator.comparingDouble(Product::getPrice))
                .limit(3)
                .toList();
        result.forEach(o -> log.info(o.toString()));
    }

    @Test
    @DisplayName("tack7: Получите 2 самых последних размещенных заказа")
    void tack7() {
        List<Order> result = orderRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Order::getOrderDate).reversed())
                .limit(2)
                .toList();
        result.forEach(o -> log.info(o.toString()));
    }

    @Test
    @DisplayName("tack8: Получите список товаров, которые были заказаны 22 марта 2022 года")
    void tack8() {
        List<Product> result = new ArrayList<>();
        orderRepository.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2022, 3, 22)))
                .forEach(order -> result.addAll(order.getProducts()));
        result.forEach(o -> log.info(o.toString()));
    }

    @Test
    @DisplayName("tack9: Рассчитайте общую сумму всех заказов, размещенных в апреле 2022 года")
    void tack9() {
        double result = orderRepository.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2022, 3, 22)))
                .mapToDouble(order -> order.getProducts()
                        .stream()
                        .mapToDouble(Product::getPrice)
                        .sum())
                .sum();
        log.info("Общая сумма = " + result);
    }

    @Test
    @DisplayName("tack10: Рассчитайте среднюю цену всех заказов, размещенных 10 апреля 2022 года")
    void tack10() {
        double result = orderRepository.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2022, 4, 10)))
                .mapToDouble(order -> order.getProducts()
                        .stream()
                        .mapToDouble(Product::getPrice)
                        .sum())
                .sum() / orderRepository.findAll()
                .stream()
                .filter(order -> order.getOrderDate().isEqual(LocalDate.of(2022, 4, 10)))
                .count();
        log.info("Средняя цена = " + result);
    }

    @Test
    @DisplayName("tack11: Получить сводную статистику по всем товарам категории \"Книги\"")
    void tack11() {
        DoubleSummaryStatistics statistics = productRepository.findAll()
                .stream()
                .filter(product -> Objects.equals(product.getCategory(), "Книги"))
                .collect(Collectors.summarizingDouble(Product::getPrice));
        log.info("count = {}, average = {}, max = {}, min = {}, sum = {}", statistics.getCount(), statistics.getAverage(), statistics.getMax(), statistics.getMin(), statistics.getSum());
    }

    @Test
    @DisplayName("tack12: Получить ассоциативный массив с order_id и количеством product в заказе")
    void tack12() {
        Map<Long, Integer> result = orderRepository.findAll()
                .stream()
                .collect(Collectors.toMap(Order::getId, order -> order.getProducts().size()));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack13: Получить ассоциативный массив данных customer список orders")
    void tack13() {
        Map<Customer, List<Order>> result = customerRepository.findAll()
                .stream()
                .collect(Collectors.toMap(
                        customer -> customer,
                        customer -> orderRepository.findAll()
                                .stream()
                                .filter(order -> order.getCustomer() == customer)
                                .toList()));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack14: Получить ассоциативный массив данных customer_id и список order_id)")
    void tack14() {
        HashMap<Long, List<Long>> result = (HashMap<Long, List<Long>>)
                customerRepository.findAll()
                        .stream()
                        .collect(Collectors.toMap(
                                Customer::getId,
                                customer -> orderRepository.findAll()
                                        .stream()
                                        .filter(order -> order.getCustomer() == customer)
                                        .map(Order::getId)
                                        .toList())
                        );
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack15: Получить ассоциативный массив данных с указанием заказа и его общей цены")
    void tack15() {
        Map<Order, Double> result = orderRepository.findAll()
                .stream()
                .collect(Collectors
                        .toMap(order -> order,
                                order -> order.getProducts()
                                        .stream()
                                        .mapToDouble(Product::getPrice)
                                        .sum()));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack16: Получить ассоциативный массив данных с указанием заказа и его общей цены используя reduce")
    void tack16() {
        Map<Long, Double> result = orderRepository.findAll()
                .stream()
                .collect(Collectors.toMap(Order::getId,
                        order -> productRepository.findAll()
                                .stream()
                                .filter(product -> order.getProducts().contains(product))
                                .map(Product::getPrice)
                                .reduce(0.0, Double::sum)));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack17: Получить ассоциативный массив данных названия продукта по категориям")
    void tack17() {
        Map<String, List<String>> result = productRepository.findAll()
                .stream()
                .map(Product::getCategory)
                .distinct()
                .collect(Collectors.toMap(category -> category,
                        category -> productRepository.findAll()
                                .stream()
                                .filter(product -> Objects.equals(product.getCategory(), category))
                                .map(Product::getName)
                                .toList()));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack18: Получить самый дорогой товар в каждой категории")
    void tack18() {
        Map<String, Optional<Product>> result = productRepository.findAll()
                .stream()
                .map(Product::getCategory)
                .distinct()
                .collect(Collectors.toMap(category -> category,
                        category -> productRepository.findAll()
                                .stream()
                                .filter(product -> Objects.equals(product.getCategory(), category))
                                .max(Comparator.comparingDouble(Product::getPrice))
                ));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack19: Получить самый дорогой товар (по наименованию) в каждой категории")
    void tack19() {
        Map<String, String> result = productRepository.findAll()
                .stream()
                .map(Product::getCategory)
                .distinct()
                .collect(Collectors.toMap(category -> category,
                        category -> productRepository.findAll()
                                .stream()
                                .filter(product -> Objects.equals(product.getCategory(), category))
                                .max(Comparator.comparingDouble(Product::getPrice))
                                .map(Product::getName).orElse("Не найдено")
                ));
        log.info(result.toString());
    }

    @Test
    @DisplayName("tack20: Рассчитайте общую сумму всех заказов за все время")
    void tack20() {
        double result = orderRepository.findAll()
                .stream()
                .mapToDouble(order -> order.getProducts()
                        .stream()
                        .mapToDouble(Product::getPrice)
                        .sum())
                .sum();

        log.info(String.valueOf(result));
    }
}
